biboumi (9.0-1) unstable; urgency=medium

  [ Alberto Luaces ]
  * New release (Closes: Bug#957037 and Bug#976356).

 --

biboumi (8.3-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Fix typo in watch file usage comment.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Jun 2018 22:40:30 +0200

biboumi (8.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Dominik George ]
  * Remove empty /usr/lib from package.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Fix typo in usage comment.
    + Update download pattern.
  * Modernize cdbs: Stop resolve build-dependencies in rules file.
  * Update package relations:
    + Tighten to Build-depend (explicitly and) versioned on gcc.
  * Update Vcs-* fields: Source hosted at Salsa.debian.org now.
  * Declare compliance with Debian Policy 4.1.4.
  * Unfuzz patch 2001.
  * Add patch 1002 to allow identd with systemd.
    Closes: Bug#895944. Thanks to Dominik George.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 May 2018 16:42:16 +0200

biboumi (7.2-2) unstable; urgency=medium

  * Update package relations:
    + Build-depend on libbotan-2-dev (not libgcrypt20-dev).
      Closes: Bug#857658. Thanks to Michel Le Bihan.
  * Drop custom (and wrong) Botan-related build options.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 Mar 2018 01:09:42 +0100

biboumi (7.2-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Fix ship with empty dir /var/lib/biboumi to get it removed on purge.
    Closes: Bug#886932. Thanks to Andreas Beckmann.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 Feb 2018 20:03:54 +0100

biboumi (7.1-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    Closes: Bug#887838. Thanks to W. Martin Borgert.

  [ Jonas Smedegaard ]
  * Update package relations: Build-depend on libpq-dev.
  * Add patch 1001 to tighten shell code.
  * Install sqlite dumb script as example script.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 23 Jan 2018 11:29:57 +0100

biboumi (6.1-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Fix update list of cmake files in the public-domain.
      Closes: Bug#877536. Thanks to Florent Le Coz <louiz@louiz.org>.
  * Build with user/group _biboumi, and create account in postinst:
    Newly enabled SQlite support requires a real home.
    Closes: 877533. Thanks to Vasudev Kamath and Florent Le Coz.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 04 Oct 2017 23:58:22 +0200

biboumi (6.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update package relations: Build-depend on libsqlite3-dev.
  * Unfuzz patch 2001.
  * Declare compliance with Debian Policy 4.1.1.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Sep 2017 01:30:26 +0200

biboumi (5.0-3) unstable; urgency=medium

  * Have binary package provide virtual package ident-server.
  * Fix watch file.
  * Declare compliance with Debian Policy 4.1.0.
  * Tighten lintian overrides regarding License-Reference.
  * Advertise DEP3 format in patch headers.
  * Update copyright info: Fix include License section BSD-3-clause.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 Sep 2017 14:50:39 +0200

biboumi (5.0-2) unstable; urgency=medium

  * Fix build with option -DCMAKE_BUILD_TYPE=Release.
    Closes: Bug#866225. Thanks to Florent Le Coz.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 28 Jun 2017 20:26:57 +0200

biboumi (5.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Add identd server.
    + Add persistent option for channels, to behave like an IRC bouncer.
    + Use udns library (not c-ares) for asynchronous DNS resolution.
    + Update MAM implementation to version 6.0 (namespace mam:2).
    + Limit MAM to 100 messages per channel by default.
    + Properly handle multiline topics.
    + Support overriding configuration options by environment values.
    + Support customizing Botan TLS policies per IRC server.
    + IRC channel config form now available via MUC config.
    + Notices starting with "#channel" now treated as welcome messages.

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 4.0.0.
  * Modernize Vcs-Browser field:
    + Use git (not cgit) in path.
  * Update package relations:
    + Build-depend on libudns-dev (not libc-ares-dev).
    + Build-depend on libgcrypt20-dev.
    + Stop build-depend on libbotan1.10-dev (unused: Too old).
  * Drop patch 2002: Fixed upstream.
  * Unfuzz patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 24 Jun 2017 10:15:23 +0200

biboumi (4.3-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Works with botan 2.x (not only botan 1.11.x).
    + Fix build without LiteSQL available.
    + Fix segmentation fault connecting to IRC server on undefined port.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Fix mangle upstream filename (lacked versioning, confusing uscan).
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Stop build-depend on licensecheck.
  * Drop patch cherry-picked upstream and now applied.
  * Drop patch cherry-picked upstream but now mysteriously gone from
    upstream master branch.
  * Update copyright info:
    + Extend copyright for my parts to cover current year.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 21 May 2017 12:03:04 +0200

biboumi (4.0-2) unstable; urgency=medium

  * Fix set proper group (and explicitly set user too, while at it).
    Closes: Bug#854252. Thanks to Jonas Wielicki and Florent Le Coz.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Mar 2017 20:37:49 +0100

biboumi (4.0-1) unstable; urgency=medium

  [ Vasudev Kamath & Jonas Smedegaard ]
  * Initial Release.
    Closes: bug#841773.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 07 Jan 2017 12:57:41 +0100
